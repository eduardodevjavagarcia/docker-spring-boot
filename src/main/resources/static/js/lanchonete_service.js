'use strict';

lanchoneteApp.factory('lanchoneteService', [
		'$http',
		'$q',
		function($http, $q) {

			return {

				allLanches : function() {

					return $http.get('/lanchonete/api/lanches').then(
							function(response) {
								return response.data;
							}, function(errResponse) {
								console.error('Erro ao buscar os lanches');
								return $q.reject(errResponse);
							});
				},
				allIngredientes : function() {

					return $http.get('/lanchonete/api/ingredientes').then(
							function(response) {
								return response.data;
							}, function(errResponse) {
								console.error('Erro ao buscar os lanches');
								return $q.reject(errResponse);
							});
				}
			};

		} ]);