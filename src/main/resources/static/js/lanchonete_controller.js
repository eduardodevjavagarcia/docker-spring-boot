'use strict';

lanchoneteApp.controller('lanchoneteController', [
		'$scope',
		'$window',
		'$log',
		'lanchoneteService',
		function($scope, $window, $log, lanchoneteService) {
			
			$scope.reverse = true;

			var self = this;
			self.lanches = [];
			self.ingredientes = [];
			
			self.qtd;
			
			self.qtdAlface = 0;
			self.qtdBacon = 0;
			self.qtdHamburguer = 0;
			self.qtdOvo = 0;
			self.qtdQueijo = 0;
			
			self.totalLanche = 0.0;
			self.valorIngrediente = 0;
			self.exibirValorTotal = false;
			
			self.calculateTotal = function(qtd, vlrUnitario) {
				self.totalLanche = self.vlrUnitario1 + self.vlrUnitario2+ self.vlrUnitario3 + self.vlrUnitario4 + self.vlrUnitario5;
				self.exibirValorTotal = true;
			};
			
			self.pedirOutroLanche = function() {
				self.totalLanche = 0.0;
				self.exibirValorTotal = false;
			};

			self.allLanches = function() {

				lanchoneteService.allLanches().then(
						function(response) {
							self.lanches = response;
						}, function(errResponse) {
							console.log('Erro: ' + errResponse);
						});

			};
			
			self.allIngredientes = function() {

				lanchoneteService.allIngredientes().then(
						function(response) {
							self.ingredientes = response;
						}, function(errResponse) {
							console.log('Erro: ' + errResponse);
						});

			};

			self.allLanches();
			
			self.allIngredientes();

		} ]);