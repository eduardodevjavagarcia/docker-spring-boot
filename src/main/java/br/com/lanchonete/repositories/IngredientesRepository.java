package br.com.lanchonete.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.lanchonete.entities.Ingredientes;

@Repository
public interface IngredientesRepository extends JpaRepository<Ingredientes, Long> {
	
	

}
