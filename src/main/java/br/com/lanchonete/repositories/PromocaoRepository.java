package br.com.lanchonete.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.lanchonete.entities.Promocao;

@Repository
public interface PromocaoRepository extends JpaRepository<Promocao, Long> {

}
