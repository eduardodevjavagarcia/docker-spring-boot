package br.com.lanchonete.services;

import java.util.List;

import br.com.lanchonete.entities.Ingredientes;

public interface IngredientesService {

	List<Ingredientes> getAllIngredientes();

}
