package br.com.lanchonete.services;

import java.util.List;

import br.com.lanchonete.entities.Lanche;

public interface LancheService {

	List<Lanche> getAllLanches();

}
