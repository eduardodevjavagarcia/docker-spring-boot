package br.com.lanchonete.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.lanchonete.entities.Ingredientes;
import br.com.lanchonete.repositories.IngredientesRepository;
import br.com.lanchonete.services.IngredientesService;

@Service("ingredientesService")
@Transactional
public class IngredientesServiceImpl implements IngredientesService {
	
	@Autowired
	private IngredientesRepository ingredientesRepository;

	@Override
	public List<Ingredientes> getAllIngredientes() {
		return ingredientesRepository.findAll();
	}

}
