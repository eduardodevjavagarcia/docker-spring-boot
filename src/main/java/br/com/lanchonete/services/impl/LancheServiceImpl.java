package br.com.lanchonete.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.lanchonete.entities.Lanche;
import br.com.lanchonete.repositories.LancheRepository;
import br.com.lanchonete.services.LancheService;

@Service("lancheService")
@Transactional
public class LancheServiceImpl implements LancheService {

	@Autowired
	private LancheRepository lancheRepository;

	@Override
	public List<Lanche> getAllLanches() {
		return lancheRepository.findAll();
	}

}
