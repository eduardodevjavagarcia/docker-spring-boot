package br.com.lanchonete.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "lanche")
public class Lanche implements Serializable {

	private static final long serialVersionUID = -7768114692025352501L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nome", nullable = false)
	private String nome;

	@OneToMany
	@JoinTable(name = "LANCHE_INGREDIENTES", joinColumns = @JoinColumn(name = "LANCHE_ID"), inverseJoinColumns = @JoinColumn(name = "INGREDIENTES_ID"))
	private Set<Ingredientes> ingredientes;

	@Transient
	private BigDecimal valorLanche;

	@Transient
	private String listaIngredientes;

	public BigDecimal getValorLanche() {
		return getIngredientes().stream().filter(Objects::nonNull).filter(ingrediente -> ingrediente.getValor() != null)
				.map(Ingredientes::getValor).reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Set<Ingredientes> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(Set<Ingredientes> ingredientes) {
		this.ingredientes = ingredientes;
	}

	public void setValorLanche(BigDecimal valorLanche) {
		this.valorLanche = valorLanche;
	}

	public String getListaIngredientes() {

		List<String> ingredientes = getIngredientes().stream().filter(Objects::nonNull)
				.filter(ingrediente -> ingrediente.getNome() != null).map(Ingredientes::getNome)
				.collect(Collectors.toList());

		return String.join(",", ingredientes);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lanche other = (Lanche) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Lanche [nome=" + nome + ", valorLanche=" + valorLanche + ", listaIngredientes=" + listaIngredientes
				+ "]";
	}

}
