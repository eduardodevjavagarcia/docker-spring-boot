package br.com.lanchonete.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

	@GetMapping(value = { "/", "lanchonete" })
	public String index() {
		return "index/index";
	}

	@GetMapping(value = { "/cardapio" })
	public String cardapio() {
		return "cardapio/cardapio";
	}
	
	@GetMapping(value = { "/lanchepersonalizado" })
	public String lanchepersonalizado() {
		return "lanchepersonalizado/lanche_personalizado";
	}

}
