package br.com.lanchonete.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.lanchonete.entities.Ingredientes;
import br.com.lanchonete.entities.Lanche;
import br.com.lanchonete.services.IngredientesService;
import br.com.lanchonete.services.LancheService;

@RestController
@RequestMapping("/api")
public class LanchoneteController {

	@Autowired
	private LancheService lancheService;

	@Autowired
	private IngredientesService ingredientesService;

	@GetMapping(path = "/lanches")
	public ResponseEntity<List<Lanche>> getAllLanches() {
		List<Lanche> lanches = lancheService.getAllLanches();
		if (lanches.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Lanche>>(lanches, HttpStatus.OK);
	}

	@GetMapping(path = "/ingredientes")
	public ResponseEntity<List<Ingredientes>> getAllIngredientes() {
		List<Ingredientes> ingredientes = ingredientesService.getAllIngredientes();
		if (ingredientes.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Ingredientes>>(ingredientes, HttpStatus.OK);
	}

}
