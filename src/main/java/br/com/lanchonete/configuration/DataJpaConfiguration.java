package br.com.lanchonete.configuration;

import java.util.Properties;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableJpaRepositories(basePackages = "br.com.lanchonete.repositories", entityManagerFactoryRef = "entityManagerFactory", transactionManagerRef = "getPlatformTransactionManager")
@EnableTransactionManagement
public class DataJpaConfiguration {

	@Autowired
	private Environment environment;

	@Value("${datasource.lanchonete.maxPoolSize}")
	private int maxPoolSize;

	@Bean
	@Primary
	@ConfigurationProperties(prefix = "datasource.lanchonete")
	public DataSourceProperties dataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	public DataSource dataSource() {
		DataSourceProperties dataSourceProperties = dataSourceProperties();
		HikariDataSource dataSource = (HikariDataSource) DataSourceBuilder.create(dataSourceProperties.getClassLoader())
				.driverClassName(dataSourceProperties.getDriverClassName()).url(dataSourceProperties.getUrl())
				.username(dataSourceProperties.getUsername()).password(dataSourceProperties.getPassword())
				.type(HikariDataSource.class).build();
		dataSource.setMaximumPoolSize(maxPoolSize);
		return dataSource;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws NamingException {
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		factoryBean.setDataSource(dataSource());
		factoryBean.setPackagesToScan(new String[] { "br.com.lanchonete.entities" });
		factoryBean.setJpaVendorAdapter(getJpaAdapter());
		factoryBean.setJpaProperties(getJpaProperties());
		return factoryBean;
	}

	@Bean
	public JpaVendorAdapter getJpaAdapter() {
		HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
		return hibernateJpaVendorAdapter;
	}

	private Properties getJpaProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("datasource.lanchonete.hibernate.dialect"));
		properties.put("hibernate.hbm2ddl.auto",
				environment.getRequiredProperty("datasource.lanchonete.hibernate.hbm2ddl.method"));
		properties.put("hibernate.show_sql",
				environment.getRequiredProperty("datasource.lanchonete.hibernate.show_sql"));
		properties.put("hibernate.format_sql",
				environment.getRequiredProperty("datasource.lanchonete.hibernate.format_sql"));
		if (StringUtils.isNotEmpty(environment.getRequiredProperty("datasource.lanchonete.defaultSchema"))) {
			properties.put("hibernate.default_schema", 
					environment.getRequiredProperty("datasource.lanchonete.defaultSchema"));
		}
		
		properties.put("hibernate.hbm2ddl.import_files",
				environment.getRequiredProperty("datasource.lanchonete.hibernate.import_files"));
		
		
		return properties;
	}

	@Bean
	@Autowired
	public PlatformTransactionManager getPlatformTransactionManager(EntityManagerFactory emf) {
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(emf);
		return jpaTransactionManager;
	}
}
