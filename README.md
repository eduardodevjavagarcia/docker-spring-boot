# Lanchonete Spring Boot e Docker #

Sistema para simular uma lanchonete, mas com o intuito de mostrar o conceito de container docker com uma aplicação java usando Spring e Docker

### Tecnologias Usadas ###

As tecnologias usadas foram:
 - Java 8, Spring Boot, AngularJS, Bootstrap, Apache Maven, H2 Database Engine, Jackson, Thymeleaf, Hibernate, Spring DataJPA, Hikari, Docker Toolbox e fabric8

### Como montar o ambiente? ###

Para montar o ambiente, deve-se baixar e instalar as ferramentas abaixo:
- http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html
- http://mirror.nbtelecom.com.br/apache/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.zip
- https://download.docker.com/win/stable/DockerToolbox.exe

### Configurar ###

1 - Abri o CMD.exe ou git-bash.exe e executar os comandos:
- docker-machine create nome_maquina
- docker-machine start nome_maquina

2 - Configurando o pom.xml para a sua máquina local:
- executar o comando: docker-machine env nome_maquina
- trocar no pom.xml as linhas: <dockerHost> pela informação exibida em DOCKER_HOST e <certPath> por DOCKER_CERT_PATH trocando as barras "\" por "/"

3 - Configurar o Apache Maven nas variáveis de ambiente do Windows.

4 - Configurar o java nas variáveis de ambiente do Windows
  
### Como executar? ###
	
Para executar, bastar baixar esse aplicação clonando ela com o git ou fazendo o download do repositório e executar o comando abaixo:

mvn clean package docker:stop docker:build docker:run

# Observações #
- A versão do java deve ser a 1.8
- A aplicação java está e andamento
- Acessar a aplicação como no exemplo: http://<dockerHost>/lanchonete
- Se tiver o problema https://registry-1.docker.io/v2/, criar uma conta no site https://hub.docker.com/ e executar o comando: docker login -p (password do dockerhubd) -u (usuário do dockerhubd)